<?php 

	/**
		@author:  Coordinación Web MiPPCI
		@version: 1.0
	 */

?>

<?php get_header() ?>
<section class="Row">
	<div class="Inner">
		<div class="NotFound">
			<div class="Inner">				
				<div class="BlockTitleNotFound">
					<span class="TitleNotFound">PAGINA NO ENCONTRADA</span>
				</div>									
				<div class="ColumnOne">
					<img src="<?php echo get_template_directory_uri() ?>/static/images/404.jpg" alt="VTV"/>
				</div>	
			</div>
		</div>
	</div>
</section>
<section class="Row">
	<div class="Inner">
		<div class="LastNewsNotFound">			
			<div class="BlockTitle">
				<span class="TitleLastView">Noticias Destacadas</span>
			</div>									
			<div class="ColumnOne">
					<?php 
						$nr = 6;
		            $sticky = get_option( 'sticky_posts' );
		            rsort( $sticky );
		            $sticky = array_slice( $sticky, 0, $nr );
		        ?>
				<?php $the_query = new WP_Query(array( 'post__in' => $sticky, 'ignore_sticky_posts' => 1 )); 
				?>
				<?php if( $the_query->have_posts() ) { ?>
						<?php //print_r($the_query);?>
				        <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
				        <article class="PostColumn">
				        <a href="<?php the_permalink() ?>"><?php the_post_thumbnail( 'lsThumbOpinion' ) ?></a>
					       <span class="PostTitle">
					        	<a href="<?php the_permalink() ?>" title="<?php the_title(); ?>">
				                    <?php the_title(); ?>
				                </a>
				            </span>
				        </article>
			    		<?php endwhile; ?>
			    <?php } ?>		
					<?php  wp_reset_postdata(); ?>	
			</div>	
		</div>
	</div>
</section>

<?php get_footer() ?>