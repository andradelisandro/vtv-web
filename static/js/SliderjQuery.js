(function ($) {

	$.fn.slider = function (settings) {

		var s = $.extend({
			data:'',			// Data provide from WP_Query
			rlBtns:true,		// Right and Left Buttons
			pointersBtns:true,		// Pointers Buttons
			thumbBtns:true,		// Thumbnails Buttons
			autoMove:true,		// Auto movement through items
			time:3,
			showTitle:true,
			showExcerpt:true,
			showImage:true,
			showTime:true,
			showCats:true,
			showReadMore:false,
			showThumbTitle:false,
		}, settings);

		var timer;

		return this.each(function(){

			this.appendChild(createSlider(s.data,this.className));

			$(document).on('ready',function(){

				autoMove();

				$('.Thumb, .Thumb-title').on('click',function(){
					movement($(this).data('pos'));
					clearInterval(timer);
					autoMove();
				});

				$('.Pointer').on('click',function(){
					movement($(this).data('pos'));
					clearInterval(timer);
					autoPlay();
				});

				$('.RBtn').on('click',function(){
					movement($(this).attr('data-nextmove'));
					clearInterval(timer);
					autoMove();
				});

				$('.LBtn').on('click',function(){
					movement($(this).attr('data-nextmove'));
					clearInterval(timer);
					autoMove();
				});

			});

		});

		function nextMovement(){

			var nextS = $('div[class="Slide Slide-current"]');
			var p = parseInt(nextS.data('pos'));

			if (p==s.data.length-1){
				p = 0;
			} else {
				p = p + 1;
			}

			return p;

		}

		function autoMove(){

			if (s.autoMove===true) {
				timer = setInterval(function(){

					p = nextMovement();
					movement(p);

				},s.time*1000);
			}

		}

		function createSlider(d,Slide){

			var decodeHtmlEntity = function(str) {
			  return str.replace(/&#(\d+);/g, function(match, dec) {
			    return String.fromCharCode(dec);
			  });
			};

			var encodeHtmlEntity = function(str) {
			  var buf = [];
			  for (var i=str.length-1;i>=0;i--) {
			    buf.unshift(['&#', str[i].charCodeAt(), ';'].join(''));
			  }
			  return buf.join('');
			};			
			var inner = document.createElement('div');
			inner.className = Slide+'-inner';

			var nav = document.createElement('div');
			nav.className = 'Nav';

			var rlbtns = document.createElement('div');
			rlbtns.className = 'RLBtns';

			var rbtn = document.createElement('div');
			rbtn.className = 'RBtn';
			rbtn.setAttribute('data-nextmove','1');

			var lbtn = document.createElement('div');
			lbtn.className = 'LBtn';
			lbtn.setAttribute('data-nextmove',d.length-1);

			rlbtns.appendChild(lbtn);
			rlbtns.appendChild(rbtn);

			var pointersBtns = document.createElement('ul');
			pointersBtns.className = 'Pointers';

			for (var i = 0; i < d.length; i++) {

				//Creación del SLIDE
				var slide = document.createElement('div');
				slide.id = 'slide-'+i;
				slide.className = (i==0)?'Slide Slide-current':'Slide';
				slide.setAttribute('data-pos',i);

				var info = document.createElement('div');
				info.className = 'info';

				// Creación de la Imagen con Link
				var aimg = document.createElement('a');
				aimg.href = d[i].link;
				aimg.innerHTML = d[i].img;

				// Creación del Título con Link
				var title = document.createElement('span');
				title.className = 's-title';

				var atitle = document.createElement('a');
				atitle.href = d[i].link;
				atitle.appendChild(document.createTextNode(decodeHtmlEntity(d[i].title)));
				title.appendChild(atitle);

				// Creación del Sumario
				var excerpt = document.createElement('span');
				excerpt.className = 's-excerpt';
				excerpt.appendChild(document.createTextNode(d[i].excerpt));

				// Creación de la Fecha de Publicación
				var time = document.createElement('span');
				time.className = 's-time DatePost';
				time.appendChild(document.createTextNode(d[i].time));

				// Creación de Categoría de la Publicación
				var cats = document.createElement('span');
				cats.className = 's-category cat-default cat-'+d[i].cats[0].slug;
				cats.appendChild(document.createTextNode(d[i].cats[0].name));

				// Creación del Botón "Leer más"
				var readmore = document.createElement('a');
				readmore.className = 's-readmore';
				readmore.href = d[i].link;
				readmore.appendChild(document.createTextNode('Ver +'));


				if (s.showImage===true) slide.appendChild(aimg);
				if (s.showCats===true) info.appendChild(cats);
				if (s.showTime===true) info.appendChild(time);
				if (s.showTitle===true) info.appendChild(title);
				if (s.showExcerpt===true) info.appendChild(excerpt);
				if (s.showReadMore===true) info.appendChild(readmore);

				slide.appendChild(info);

				inner.appendChild(slide);

				var thumb = document.createElement('div');
				thumb.id = 'thumb-'+i;
				thumb.className = (i==0)?'Thumb Thumb-current':'Thumb';
				thumb.setAttribute('data-pos',i);

				thumb.innerHTML = d[i].thumb;

				var thumbTitle = document.createElement('span');
				thumbTitle.className = 'Thumb-title';
				thumbTitle.appendChild(document.createTextNode(d[i].title));
				thumb.appendChild(thumbTitle);

				if (s.thumbBtns===true) nav.appendChild(thumb);

				var pointer = document.createElement('li');
				pointer.id = "Pointer-"+i;
				pointer.className = (i==0)?'Pointer PointerCurrent':'Pointer';
				pointer.setAttribute('data-pos',i);

				pointersBtns.appendChild(pointer);

			};

			inner.appendChild(nav);
			if (s.rlBtns===true) inner.appendChild(rlbtns);
			if (s.pointersBtns===true) inner.appendChild(pointersBtns);

			return inner;

		}

		function movement(p){

			var nextS = $('div[id="slide-'+p+'"]');
			var currentS = $('div[class="Slide Slide-current"]');
			var nextT = $('div[id="thumb-'+p+'"]');
			var currentT = $('div[class="Thumb Thumb-current"]');
			var nextP = $('li[id="Pointer-'+p+'"]');
			var currentP = $('li[class="Pointer PointerCurrent"]');

			if (!nextS.hasClass('Slide-current')){

				currentS.animate({opacity:0,},150,function() {
					$(this).removeClass('Slide-current');
				});

				currentT.animate({opacity:.6,},150,function() {
					$(this).removeClass('Thumb-current');
				});

				currentP.animate({opacity:1,},150,function() {
					$(this).removeClass('PointerCurrent');
				});

				nextS.animate({opacity:1},150,function(){
					$(this).addClass('Slide-current');
				});

				nextT.animate({opacity:1},150,function(){
					$(this).addClass('Thumb-current');
				});

				nextP.animate({opacity:1},150,function(){
					$(this).addClass('PointerCurrent');
				});

				if (s.rlBtns===true){

					pr = parseInt(p) + 1;
					pl = parseInt(p) - 1;

					if (pr > s.data.length-1) pr = 0;
					if (pl < 0) pl = s.data.length-1;

					$('.RLBtns .RBtn').attr('data-nextmove',pr);
					$('.RLBtns .LBtn').attr('data-nextmove',pl);

				}

			}

		}

	}

}(jQuery));