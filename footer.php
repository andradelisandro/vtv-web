<?php 

	/**
		@author:  Coordinación Web MIPPCI
		@version: 1.0
	 */

?>	

			</div>
		</section>
		<footer id="Footer">
			<div class="ContentFooter">			
				<div class="ColumnOne">	
					<div class="RowOne">
						<ul class="IconInteres">
							<li class="Alba"><a href="http://www.albatv.org/" target="_blank"></a></li>
							<li class="Antv"><a href="http://www.antv.gob.ve/" target="_blank"></a></li>
							<li class="AvilaTv"><a href="http://www.avilatv.gob.ve/" target="_blank"></a></li>
							<li class="Avn"><a href="http://www.avn.info.ve/" target="_blank"></a></li>
							<li class="LaRadioDelSur"><a href="https://laradiodelsur.com.ve/" target="_blank"></a></li>
							<li class="Rnv"><a href="http://rnv.gob.ve/" target="_blank"></a></li>
							<li class="Telesur"><a href="http://www.telesurtv.net/" target="_blank"></a></li>
							<li class="Teves"><a href="http://www.tves.gob.ve/" target="_blank"></a></li>
							<li class="Vive"><a href="http://www.vive.gob.ve/" target="_blank"></a></li>
							<li class="YVKMundial"><a href="http://www.radiomundial.com.ve/" target="_blank"></a></li>
						</ul>
					</div>
					<div class="RowTwo">
						<div class="ColumnOne">				
							<ul class="MenuElCanal">
								<li class="ElCanal">EL CANAL </li>
								<!--<li class="QuienesSomos"><a href="#" target="_blank"> Quienes Somos </a></li>-->
								<!--<li class="Organigrama"><a href="#" target="_blank"> Organigrama </a></li>-->
								<li class="Contacto"><a href="<?php bloginfo('url') ?>/contacto/" target="_blank"> Contacto </a></li>
								<!--<li class="ProgramacionDiaria"><a href="#" target="_blank"> Programación Diaria </a></li>-->
								<li class="Historia"><a href="<?php bloginfo('url') ?>/historia/" target="_blank"> Historia </a></li>
								<li class="CarreraVtv"><a href="<?php bloginfo('url') ?>/carrera/" target="_blank"> Carrera en VTV </a></li>								
							</ul>
						</div>
						<div class="ColumnTwo">
							<ul class="MenuMultimedia">
								<li class="Multimedia"> MULTIMEDIA </li>
								<li class="Galeria"><a href="<?php bloginfo('url') ?>/noticias/galerias" target="_blank"> Galeria </a></li>
								<!--li class="Video"><a href="#" target="_blank"> Video </a></li-->					
								<li class="Programas"><a href="<?php bloginfo('url') ?>/noticias/programas" target="_blank"> Programas </a></li>
								<!--li class="Promociones"><a href="#" target="_blank"> Promociones </a></li-->
								<li class="NoticierosDigitales"><a href="https://www.youtube.com/playlist?list=PLmRW3NoV9u0yn3cjNCj4-Ex6Afx0JDBA9" target="_blank"> Vamos 2.0 </a></li>
							</ul>						
						</div>
						<div class="ColumnThree">
							<ul class="MenuNoticias">
								<li class="Noticias"> NOTICIAS </li>
								<li class="Nacionales"><a href="<?php bloginfo('url') ?>/noticias/nacionales" target="_blank"> Nacionales </a></li>
								<li class="Internacionales"><a href="<?php bloginfo('url') ?>/noticias/internacionales" target="_blank"> Internacionales </a></li>					
								<li class="Economia"><a href="<?php bloginfo('url') ?>/noticias/economia" target="_blank"> Economia </a></li>
								<li class="Deportes"><a href="<?php bloginfo('url') ?>/noticias/deportes" target="_blank"> Deportes </a></li>
								<li class="Cultura"><a href="<?php bloginfo('url') ?>/noticias/cultura" target="_blank"> Cultura </a></li>
								<li class="SaludCienciaTecnologia"><a href="<?php bloginfo('url') ?>/noticias/saludceinciaytecnologia"target="_blank"> Salud Ciencia y Tecnologia </a></li>
							</ul>							
						</div>
					</div>
					<div class="RowThree">
						<div class="ColumnOne">				
							<ul class="MenuSocial">								
								<li class="Instagram"><a href="https://www.instagram.com/vtvcanal8/" title="Instagram" target="_blank"></a></li>
								<li class="Twitter"><a href="https://twitter.com/vtvcanal8" title="Twitter" target="_blank"></a></li>
								<li class="Vine"><a href="https://vine.co/u/1134686784320671744" title="Vine" target="_blank"></a></li>
								<li class="Facebook"><a href="https://es-la.facebook.com/VTVtuCanal" title="Facebook" target="_blank"></a></li>
								<li class="YouTube"><a href="https://www.youtube.com/user/MultimedioVTV" title="YouTube" target="_blank"></a></li>
								<li class="Vtv"><a href="http://historico.vtv.gob.ve/" title="Historicos de VTV" target="_blank"></a></li>
							</ul>
						</div>
						<div class="ColumnTwo">
							<div class="Search"><?php get_search_form();?></div>
						</div>
					</div>
				</div>
				<div class="ColumnTwo">
					<?php dynamic_sidebar('banner-footer'); ?>
				</div>
			</div>		
		</footer>
	<?php wp_footer() ?>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-84740153-1', 'auto');
	  ga('send', 'pageview');
	</script>
</body>
</html>
