/**
	@author: MIPPCI
	@version: 1.0
 */

var gulp	= require('gulp');
var order	= require('gulp-order');
var sass	= require('gulp-ruby-sass');
var concat	= require('gulp-concat');
var csso	= require('gulp-csso');
var prefixer	= require('gulp-autoprefixer');
var sourcemaps	= require('gulp-sourcemaps');
var minifycss = require('gulp-minify-css');
var uglify = require('gulp-uglify');

gulp.task('compileSass',function(){

	return sass('resources/sass/**/*.scss',{sourcemap: true, noCache:true})
		.on('error', sass.logError)
		.pipe(sourcemaps.write('maps', {
			includeContent: false,
			sourceRoot: 'source'
		}))
		.pipe(gulp.dest('static/css/'));

});

gulp.task('concatsStyles',function(){

	return gulp.src('static/css/**/*.css')
		.pipe(order([
			"config.css",
			"reset.css",
			"header.css",
			"global.css",
			"footer.css",
			"*.css",
			]))
		.pipe(csso(true))
		.pipe(sourcemaps.init())
		.pipe(concat('style.css'))
		.pipe(prefixer({
			browsers: ['last 10 versions'],
			cascade: false
		}))
		.pipe(csso())
		.pipe(minifycss())
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('./'));

});
/** Minificar de CSS Manual ya que no estaba desarrollado **/
/*gulp.task('minify-css', function () {
  gulp.src('style.css')
  .pipe(concat('style.css'))
  .pipe(minifycss())
  .pipe(gulp.dest('./'))
});*/
gulp.task('default',function(){
	gulp.watch('resources/sass/**/*.scss',['compileSass']);
	gulp.watch('static/css/**/*.css',['concatsStyles']);
});

/** Minificar de Javascript Manual Recomendacion de https://developers.google.com/speed/pagespeed/insights/ **/
gulp.task('optimizar-js', ['minify-jqueryMarquee-js', 'minify-SliderjQuery-js']);

gulp.task('minify-jqueryMarquee-js', function () {
  gulp.src('static/js/jqueryMarquee.js')
	//.pipe(concat('jqueryMarquee.min.js'))
  .pipe(uglify())
  .pipe(gulp.dest('./static/js/min/'))
});

gulp.task('minify-SliderjQuery-js', function () {
  gulp.src('static/js/SliderjQuery.js')
	//.pipe(concat('SliderjQuery.min.js'))
  .pipe(uglify())
  .pipe(gulp.dest('./static/js/min/'))
});
