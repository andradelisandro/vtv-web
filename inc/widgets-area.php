<?php 

// Widgets Area
function widgets_area() {

	$Widget = 'Widget';
	$WidgetTitle = 'WidgetTitle';
	//$WidgetContent = 'WidgetContent';

	$WidgetBase = array(
		'before_widget' => '<div class="'.$Widget.'">',
		
		'before_title' => '<span class="'.$WidgetTitle.'">',
		//'after_title' => '</span><div class="'.$WidgetContent.'">',
		'after_title' => '</span>',

		//'after_widget' => '</div><!-- END WIDGET CONTENT --></div><!-- END WIDGET -->',
		'after_widget' => '</div>',
	);

	/**
		Áreas de Widgets para la Página Principal
	 */

	$IndexSidebar = array(
		'id' => 'index-sidebar',
		'name' => 'Sidebar del Index',
		'description' => 'Área de imágenes para el sidebar del Index',
		'empty_title'=> '',
	);

	register_sidebar(array_merge($WidgetBase, $IndexSidebar));

	$IndexBannerOne = array(
		'id' => 'index-banner-1',
		'name' => 'Primer Banner del Index',
		'description' => 'Primer Área de banner para el Index',
		'empty_title'=> '',
	);

	register_sidebar(array_merge($WidgetBase, $IndexBannerOne));

	$IndexBannerTwo = array(
		'id' => 'index-banner-2',
		'name' => 'Segundo Banner del Index',
		'description' => 'Segunda Área de banner para el Index',
		'empty_title'=> '',
	);

	register_sidebar(array_merge($WidgetBase, $IndexBannerTwo));

	$IndexBannerThree = array(
		'id' => 'index-banner-3',
		'name' => 'Tercer Banner del Index',
		'description' => 'Tercer Área de banner para el Index',
		'empty_title'=> '',
	);

	register_sidebar(array_merge($WidgetBase, $IndexBannerThree));

	$IndexBannerFour = array(
		'id' => 'index-banner-4',
		'name' => 'Cuarto Banner del Index',
		'description' => 'Cuarta Área de banner para el Index',
		'empty_title'=> '',
	);

	register_sidebar(array_merge($WidgetBase, $IndexBannerFour));

	$IndexBannerFive = array(
		'id' => 'index-banner-5',
		'name' => 'Quinto Banner del Index',
		'description' => 'Quinta Área de banner para el Index',
		'empty_title'=> '',
	);

	register_sidebar(array_merge($WidgetBase, $IndexBannerFive));


	$IndexBannerSix = array(
		'id' => 'index-banner-6',
		'name' => 'Banner de la Agenda Económica',
		'description' => 'Área de banner para la Agenda Económica',
		'empty_title'=> '',
	);

	register_sidebar(array_merge($WidgetBase, $IndexBannerSix));

	$IndexBannerSeven = array(
		'id' => 'index-banner-7',
		'name' => 'Sexto Banner del Index',
		'description' => 'Sexta Área de banner para el Index',
		'empty_title'=> '',
	);

	register_sidebar(array_merge($WidgetBase, $IndexBannerSeven));

		$IndexBannerFooter = array(
		'id' => 'banner-footer',
		'name' => 'Banner del Footer',
		'description' => 'Área unica de banner para el footer',
		'empty_title'=> '',
	);

	register_sidebar(array_merge($WidgetBase, $IndexBannerFooter));

		$IndexBannerHeader = array(
		'id' => 'banner-header',
		'name' => 'Banner del Header',
		'description' => 'Área unica de banner para el Header',
		'empty_title'=> '',
	);

	register_sidebar(array_merge($WidgetBase, $IndexBannerHeader));


}

add_action( 'widgets_init', 'widgets_area' );