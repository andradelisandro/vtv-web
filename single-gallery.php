<?php 

	/**
		@author:  Coordinación Web MiPPCI
		@version: 1.0
	 */

?>

<?php get_header() ?>
<?php nx_set_post_views(get_the_ID())?>
<div class="RowOne">
	<div class="Inner">
		<div class="ContentGallery">
			<div class="InnerGallery">
				<?php if ( have_posts() ): while( have_posts() ): the_post() ?>
				<div class="SinglePostGallery">
						<div class="PostTitleGallery"><?php the_title() ?>	</div>
						<div class="PostContentGallery"><?php the_content() ?></div>
						<div class="PostDateGallery">Publicado el <?php the_time( 'd/M Y' )?> a las: <?php the_time('g:i a'); ?></div>

				</div>
				<?php endwhile; endif ?>
			</div>
		</div>
	</div>
</div>


<?php get_footer() ?>