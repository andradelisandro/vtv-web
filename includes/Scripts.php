<?php

	/**
		@author:  Coordinación Web MiPPCI
		@version: 1.0
	 */

// Register Scripts
function Scripts() {

	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'GFonts', get_template_directory_uri() . '/static/js/GFonts.js', array(), false, true );
	wp_enqueue_script( 'SliderjQuery', get_template_directory_uri() . '/static/js/min/SliderjQuery.js', array(), false, true );
	wp_enqueue_script( 'jqueryMarquee', get_template_directory_uri() . '/static/js/min/jqueryMarquee.js', array(), false, true );

}

// Hook into the 'wp_enqueue_scripts' action
add_action('wp_enqueue_scripts', 'Scripts');
