<?php 

	/**
		@author:  Coordinación Web MiPPCI
		@version: 1.0
	 */

?>

<!DOCTYPE html>
<html <?php language_attributes() ?>>

<head>

	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="identifier-url" content="http://vtv.gob.ve/" />
	<meta name="title" content="VTV | Venezolana de Televisión" />
	<meta name="author" content="MippCI" />
	<meta name="robots" content="All" />
	<meta name="revisit-after" content="1" />
	<meta name="description" content="La principal televisora del Estado, con cobertura nacional en señal abierta las 24 horas y transmisión en vivo de programas informativos y de opinión - VTV" />
	<meta name="keywords" content="noticias, política, comunicación, información, medios, comunitarios, públicos, presidencia, venezuela, república bolivariana , vtv , en-vivo" />
	<meta name="copyright" content="VTV | <?php echo date('Y') ?>" />

	<?php wp_head() ?>

</head>

<body <?php body_class() ?>>
	<main id="Main">
		<div class="Inner">
			<header id="Header">
				<div class="Inner">
					<div class="Banner">
						<?php dynamic_sidebar('banner-header'); ?>
					</div>
					<nav id="Nav">
						<?php global $Main; wp_nav_menu( $Main ) ?>						
						<ul class="MenuSocial">
							<li class="Search"><?php get_search_form();?></li>
							<li class="twitter"><a href="https://twitter.com/vtvcanal8" target="_blank"></a></li>
							<li class="facebook"><a href="https://es-la.facebook.com/VTVtuCanal" target="_blank"></a></li>
							<li class="instagram"><a href="https://www.instagram.com/vtvcanal8/" target="_blank"></a></li>
							<li class="vine"><a href="https://vine.co/u/1134686784320671744" target="_blank"></a></li>
							<li class="youtube"><a href="https://www.youtube.com/user/MultimedioVTV" target="_blank"></a></li>
						</ul>
						<!--div class="Search"></div-->							
					</nav>
					<div class="MarqueeWArea">
						<?php $now = new WP_Query( array('posts_type'=>'posts', 'posts_per_page' => 10, 'meta_key' =>'NowPost', 'ignore_sticky_posts'=>1)); ?>
							<div class="Marquee">
								<div class="MarqueeTitle">AHORA</div>
								<div class="MarqueeContent" data-pauseonhover="true" data-duration="30000" data-delaybeforestart="0">
								<?php if ($now->have_posts()): while($now->have_posts()): $now->the_post();?>
									<a class="MarqueePost" href="<?php the_permalink() ?>"><?php the_title() ?></a>
								<?php endwhile; endif; wp_reset_postdata(); ?>
								</div>
							</div>
						<script type="text/javascript">
	                            jQuery(document).ready(function($){
	                                $('.MarqueeContent, .MarqueeEconomic').marquee();
	                            });
	                    </script>
					</div>
				</div>
			</header>
