<?php 

	/**
		@author:  Coordinación Web MiPPCI
		@version: 1.0
	 */
?>

<?php nx_set_post_views(get_the_ID())?>
		<div class="Block">
			<div class="Inner">
				<div class="LineView">
					<div class="BlockTitle"><span class="TitleLastView">Lo Último</span></div>
				</div>
					<div class="BlockContent">
				
					<?php $args=array('post__not_in' => array(get_the_ID()), 'showposts'=>7,'ignore_sticky_posts'=>1); ?>
					<?php $the_query = new WP_Query($args); 
					?>
					<?php if( $the_query->have_posts() ) { ?>
					        <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
						       <div class="Title">
						        	<a href="<?php the_permalink() ?>" title="<?php the_title(); ?>">
					                    <?php the_title(); ?>
					                </a>
					            </div>
				    		<?php endwhile; ?>
				    <?php } wp_reset_postdata()?>			
					
						</div>
			</div>
		</div>

			<?php dynamic_sidebar('index-sidebar'); ?>



		<div class="Block">
			<div class="Inner">
				<div class="LineView">
					<div class="BlockTitle"><span class="TitleLastView">Lo Más Leído</span></div>
				</div>
				<div class="BlockContent">				
					<?php 

					/*$args = array(
			        'posts_per_page' => 5, 
			        'meta_key' => 'nx_set_post_views', 
			        'orderby' => 'meta_value_num', 
			        'order' => 'DESC',
			        'ignore_sticky_posts'=>1
			    	);*/
			    	$nr = 7;

		            $sticky = get_option( 'sticky_posts' );
		            rsort( $sticky );
		            $sticky = array_slice( $sticky, 0, $nr );

	            $the_query = new WP_Query( array( 'post__in' => $sticky, 'ignore_sticky_posts' => 1 ) );
	            if($the_query->have_posts()): while($the_query->have_posts()): $the_query->the_post();
	               ?>
					<div class="Title">
			        	<a href="<?php the_permalink() ?>" title="<?php the_title(); ?>">
		                    <?php the_title(); ?> 
		                </a>
		                <span class="Date">
		                	<?php the_time('g:i a'); ?>
		                </span>
					 </div>  
       			<?php endwhile; endif; wp_reset_postdata(); ?>		
					
				</div>
			</div>
		</div>

	<?php

?>
