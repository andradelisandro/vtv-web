<?php 

	/*
	Template Name: Template Galería
	*/

	/**
		@author:  Coordinación Web MiPPCI
		@version: 1.0
	 */

?>
<?php get_header() ?>

<section class="Row">
	<div class="Inner">
		<div class="Gallery">
		<div class="TitleGallery">GALERÍAS</div>
			<div class="Inner ListGallery">
				<div class="ColumnOneGallery">
						<?php if (have_posts()): while (have_posts()): the_post() ?>
							<article class="PostColumn">
								<a href="<?php the_permalink() ?>"><?php the_post_thumbnail( 'lsThumb' ) ?></a>
								<span class="PostTitle">
									<a href="<?php the_permalink() ?>"><?php the_title() ?></a>
								</span>
								<span class="GalleryDate">
								<?php echo get_the_date(); ?> | <?php echo get_the_time(); ?>							
								</span>
							</article>
						<?php endwhile; endif; wp_reset_postdata() ?>
					</div>							
					<div class="NavPag"><?php the_pagination() ?></div>
				</div>			
		</div>
	</div>
</section>

<section class="Row">
<div class="Inner">
<div class="LastNews">
	<div class="Inner ContentLast">				
		<div class="BlockTitle">
			<span class="TitleLastView">Últimas noticias</span>
		</div>									
			<div class="ColumnOne">
				<?php $args=array('post__not_in' => get_option( 'sticky_posts'), 'showposts'=>6); ?>
					<?php $the_query = new WP_Query($args); 
					?>
					<?php if( $the_query->have_posts() ) { ?>
					        <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
					        <article class="PostColumn">
					        <a href="<?php the_permalink() ?>"><?php the_post_thumbnail( 'lsThumbOpinion' ) ?></a>
						       <span class="PostTitle">
						        	<a href="<?php the_permalink() ?>" title="<?php the_title(); ?>">
					                    <?php the_title(); ?>
					                </a>
					            </span>
					        </article>
				    		<?php endwhile; ?>
				    <?php }?>			
			</div>	
	</div>
</div>
</div>
</section>
<?php get_footer() ?>