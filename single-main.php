<?php 

	/**
		@author:  Coordinación Web MiPPCI
		@version: 1.0
	 */

?>

<?php get_header() ?>
<?php nx_set_post_views(get_the_ID())?>
<div class="RowOne">
	<div class="Inner">
		<div class="MainContent">
			<div class="Inner">
				<?php if ( have_posts() ): while( have_posts() ): the_post() ?>
				<article class="SinglePost">
					<header>
						<span class="PostTitle"><?php the_title() ?></span>
						<span class="PostDate">Publicado el <?php the_time( 'd/M Y' )?> a las: <?php the_time('g:i a'); ?></span>
						<span class="Author"> Por: <?php  the_author();?> </span>
					</header>
					<section class="PostContent"><?php the_content() ?></section>
				</article>
				<?php endwhile; endif ?>
			</div>
		</div>

		<div class="SidebarSingle">

			<div class="Block">
				<div class="Inner">
					<div class="LineView">
						<div class="BlockTitle"><span class="TitleLastView">Lo Último</span></div>
					</div>
						<div class="BlockContent">
						<?php $args=array(
						'post__not_in' => array(get_the_ID()), 
						'showposts'=>5,
						'ignore_sticky_posts'=>1
						); ?>
						<?php $the_query = new WP_Query($args); 
						?>
						<?php if( $the_query->have_posts() ) { ?>
						        <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
							       <div class="Title">
							        	<a href="<?php the_permalink() ?>" title="<?php the_title(); ?>">
						                    <?php the_title(); ?>
						                </a>
						            </div>
					    		<?php endwhile; ?>
					    <?php } wp_reset_postdata() ?>			
						
							</div>
				</div>
			</div>

			<div class="Block">
				<div class="Inner">
					<div class="LineView">
						<div class="BlockTitle"><span class="TitleLastView">Lo Más Leído</span></div>
					</div>
					<div class="BlockContent">				
						<?php 

						$nr = 5;

			            $sticky = get_option( 'sticky_posts' );
			            rsort( $sticky );
			            $sticky = array_slice( $sticky, 0, $nr );

						/*$args = array(
						'post_type'=>'post',
				        'ignore_sticky_posts' => 1,
				        'showposts' => 5, 
				        'meta_key' => 'nx_set_post_views', 
				        'orderby' => 'meta_value_num', 
				        'order' => 'DESC',
				    );*/
		            	$the_query = new WP_Query( array( 'post__in' => $sticky, 'ignore_sticky_posts' => 1 ) );
		            	if($the_query->have_posts()): while($the_query->have_posts()): $the_query->the_post();
		               	?>
						<div class="Title">
				        	<a href="<?php the_permalink() ?>" title="<?php the_title(); ?>">
			                    <?php the_title(); ?>
			                </a>
						 </div>  
	       				<?php endwhile; endif; wp_reset_postdata(); ?>		
						
					</div>
				</div>
			</div>

		</div>
	</div>
</div>


<?php get_footer() ?>