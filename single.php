<?php 

	/**
		@author:  Coordinación Web MiPPCI
		@version: 1.0
	 */

?>
<?php get_header() ?>

<?php

if (in_category('galerias')) { // Si el post pertenece a la cat 101 redirigir a single-pdf.php
    //die('Fin otra ediciones');
    get_template_part('single-gallery');
}  else {
    //die('Fin otras single-default');
    get_template_part('single-main');
}
?>


<?php get_footer() ?>

