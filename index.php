<?php 

	/**
		@author:  Coordinación Web MiPPCI
		@version: 1.0
	 */

?>

<?php get_header() ?>

<?php


            $nr = 5;

            $sticky = get_option( 'sticky_posts' );
            rsort( $sticky );
            $sticky = array_slice( $sticky, 0, $nr );

            $Now = new WP_Query( array( 'post__in' => $sticky, 'ignore_sticky_posts' => 1 ) );

            if ($Now->have_posts()): 
                while($Now->have_posts()): 
                    $Now->the_post();
                        $Arr[] = array(
                            'img'=>get_the_post_thumbnail(get_the_ID(),'lsThumbGalleryMax'),
                            'thumb'=>get_the_post_thumbnail(get_the_ID(), 'thumbnail'),
                            'title'=>get_the_title(),
                            'time'=>get_the_time('F j, Y g:i:s a'),
                            'excerpt'=>get_the_excerpt(),
                            'link'=>get_permalink(),
                            'cats'=>get_the_category(),
                        );

                        (array) $FeaturedPosts[] = get_the_ID();

                endwhile;
            endif;

            wp_reset_postdata();

            if (count($FeaturedPosts)<$nr):

                $LastFeatures = new WP_Query(array('category_name' => 'destacado',  'posts_per_page' => $nr-count($FeaturedPosts), 'post__not_in' => $FeaturedPosts ));

                if ($LastFeatures->have_posts()): 
                    while($LastFeatures->have_posts()): 
                        $LastFeatures->the_post();

                            $cat="";

                            $cats = get_the_category();

                            for($i=0; $i<count($cats); $i++):

                                if ($cats[$i]->slug == "destacado"):
                                    unset($cats[$i]);
                                    sort($cats);
                                endif;

                            endfor;

                            $Arr[] = array(
                                'img'=>get_the_post_thumbnail(get_the_ID(),'lsThumbGalleryMax'),
                                'thumb'=>get_the_post_thumbnail(get_the_ID(), 'thumbnail'),
                                'title'=>get_the_title(),
                                'time'=>get_the_time('F j, Y g:i:s a'),
                                'excerpt'=>get_the_excerpt(),
                                'link'=>get_permalink(),
                                'cats'=>$cats,
                            );

                            (array) $FeaturedPosts[] = get_the_ID();
                    endwhile;
                endif;

            endif;

            wp_reset_postdata();

?>

<div class="Row">
<div class="Inner">
	<div class="RowOne">
		<div class="Slider"></div>
		<script type="text/javascript">
			jQuery(document).ready(function($){
				$('.Slider').slider({
					data:<?php echo utf8_encode(json_encode($Arr)) ?>,
					rlBtns:false,
					pointersBtns:true,
					thumbBtns:false,
					autoMove:true,
					time:5,
					showTitle:true,
					showExcerpt:false,
					showImage:true,
					showTime:false,
					showCats:true,
					showReadMore:false,
				});
			});
		</script>
		<div class="streaming">
			<!--iframe width="476" height="300" src="https://www.youtube.com/embed/Trh-rWxgmjA" frameborder="0" allowfullscreen></iframe>-->
			<!--iframe width="476" height="300" src="http://www.movipbox.com/nuevo/embed/vtv/index.html?now=1467903181" frameborder="0" allowfullscreen style="background:#000"></iframe-->
			<iframe width="476" height="300" scrolling="No" src="https://player.streamingconnect.com/vtv/"></iframe>
 
		</div>
	</div>


	<div class="RowTwo">

		<div class="TitleBorder">
			<span class="CategoryTitle">Nacionales</span>
			<a class="CategoryReadMore" href="<?php bloginfo('url') ?>/noticias/nacionales">Ver Más noticias Nacionales</a>
		</div>
			<div class="Rows">
				<?php $Args = array('posts_per_page' => 1, 'category_name' => 'nacionales-fijo', 'post__not_in' => $FeaturedPosts) ?>
				<?php $the_query = new WP_Query($Args) ?>				
				<?php if ($the_query->have_posts()): 
					while ($the_query->have_posts()): 
						$the_query->the_post() ?>
							<?php (array) $idNacionales[]=get_the_ID();  ?>
							<article class="PostColumn">
								<a href="<?php the_permalink() ?>"><?php the_post_thumbnail( 'lsThumb' ) ?></a>
								<span class="PostTitle">
									<a href="<?php the_permalink() ?>"><?php the_title() ?></a>
								</span>
							</article>
					<?php endwhile; 
				endif; wp_reset_postdata() ?>
			</div>
			<?php $arrayMergeNacionales = array_merge($FeaturedPosts,$idNacionales); ?>
			<div class="Rows">
				<?php $Args = array('posts_per_page' => 2, 'category_name' => 'nacionales','post__not_in' => $arrayMergeNacionales) ?>
				<?php $the_query = new WP_Query($Args) ?>
				<?php if ($the_query->have_posts()): while ($the_query->have_posts()): $the_query->the_post() ?>
					<article class="PostColumn">
						<a href="<?php the_permalink() ?>"><?php the_post_thumbnail( 'lsThumb' ) ?></a>
						<span class="PostTitle">
							<a href="<?php the_permalink() ?>"><?php the_title() ?></a>
						</span>
					</article>
				<?php endwhile; endif; wp_reset_postdata() ?>
			</div>
				
			<div class="TitleBorder">
				<span class="CategoryTitle">Economía</span>
				<a class="CategoryReadMore" href="<?php bloginfo('url') ?>/noticias/economia">Ver Más noticias economía</a>
			</div>

			<div class="Rows">
				<?php $Args = array('posts_per_page' => 1, 'category_name' => 'economia-fijo', 'post__not_in' => $FeaturedPosts) ?>
				<?php $the_query = new WP_Query($Args) ?>
				<?php if ($the_query->have_posts()): while ($the_query->have_posts()): $the_query->the_post() ?>
					<?php (array) $idEconomia[]=get_the_ID();  ?>
					<article class="PostColumn">
						<a href="<?php the_permalink() ?>"><?php the_post_thumbnail( 'lsThumb' ) ?></a>
						<span class="PostTitle">
							<a href="<?php the_permalink() ?>"><?php the_title() ?></a>
						</span>
					</article>
				<?php endwhile; endif; wp_reset_postdata() ?>
			</div>
			<?php $arrayMergeEconomia = array_merge($FeaturedPosts,$idEconomia); ?>
			<div class="Rows">
				<?php $Args = array('posts_per_page' => 2, 'category_name' => 'economia', 'post__not_in' => $arrayMergeEconomia) ?>
				<?php $the_query = new WP_Query($Args) ?>
				<?php if ($the_query->have_posts()): while ($the_query->have_posts()): $the_query->the_post() ?>
					<article class="PostColumn">
						<a href="<?php the_permalink() ?>"><?php the_post_thumbnail( 'lsThumb' ) ?></a>
						<span class="PostTitle">
							<a href="<?php the_permalink() ?>"><?php the_title() ?></a>
						</span>
					</article>
				<?php endwhile; endif; wp_reset_postdata() ?>
			</div>

			<?php dynamic_sidebar('index-banner-1'); ?>

            <div class="TitleBorder">
                <span class="CategoryTitle">Regionales</span>
                <a class="CategoryReadMore" href="<?php bloginfo('url') ?>/noticias/regionales">Ver Más noticias regionales</a>
            </div>

            <div class="Rows">
                <?php $Args = array('posts_per_page' => 1, 'category_name' => 'regionales-fijo', 'post__not_in' => $FeaturedPosts) ?>
                <?php $the_query = new WP_Query($Args) ?>
                <?php if ($the_query->have_posts()): while ($the_query->have_posts()): $the_query->the_post() ?>
                    <?php (array) $idRegionales[]=get_the_ID();  ?>
                    <article class="PostColumn">
                        <a href="<?php the_permalink() ?>"><?php the_post_thumbnail( 'lsThumb' ) ?></a>
                        <span class="PostTitle">
                            <a href="<?php the_permalink() ?>"><?php the_title() ?></a>
                        </span>
                    </article>
                <?php endwhile; endif; wp_reset_postdata() ?>
            </div>
            <?php $arrayMergeRegionales = array_merge($FeaturedPosts,$idRegionales); ?>
            <div class="Rows">
                <?php $Args = array('posts_per_page' => 2, 'category_name' => 'regionales', 'post__not_in' => $arrayMergeRegionales) ?>
                <?php $the_query = new WP_Query($Args) ?>
                <?php if ($the_query->have_posts()): while ($the_query->have_posts()): $the_query->the_post() ?>
                    <article class="PostColumn">
                        <a href="<?php the_permalink() ?>"><?php the_post_thumbnail( 'lsThumb' ) ?></a>
                        <span class="PostTitle">
                            <a href="<?php the_permalink() ?>"><?php the_title() ?></a>
                        </span>
                    </article>
                <?php endwhile; endif; wp_reset_postdata() ?>
            </div>

			<div class="TitleBorder">
				<span class="CategoryTitle">Internacionales</span>
				<a class="CategoryReadMore" href="<?php bloginfo('url') ?>/noticias/internacionales">Ver Más noticias internacionales</a>
			</div>
			
			<div class="Rows">
				<?php $Args = array('posts_per_page' => 1, 'category_name' => 'internacionales-fijo', 'post__not_in' => $FeaturedPosts) ?>
				<?php $the_query = new WP_Query($Args) ?>
				<?php if ($the_query->have_posts()): while ($the_query->have_posts()): $the_query->the_post() ?>
					<?php (array) $idInternacionales[]=get_the_ID();  ?>
					<article class="PostColumn">
						<a href="<?php the_permalink() ?>"><?php the_post_thumbnail( 'lsThumb' ) ?></a>
						<span class="PostTitle">
							<a href="<?php the_permalink() ?>"><?php the_title() ?></a>
						</span>
						
					</article>
				<?php endwhile; endif; wp_reset_postdata() ?>
			</div>
			<?php $arrayMergeInternacionales = array_merge($FeaturedPosts,$idInternacionales); ?>
			<div class="Rows">
				<?php $Args = array('posts_per_page' => 2, 'category_name' => 'internacionales', 'post__not_in' => $arrayMergeInternacionales) ?>
				<?php $the_query = new WP_Query($Args) ?>
				<?php if ($the_query->have_posts()): while ($the_query->have_posts()): $the_query->the_post() ?>
					<article class="PostColumn">
						<a href="<?php the_permalink() ?>"><?php the_post_thumbnail( 'lsThumb' ) ?></a>
						<span class="PostTitle">
							<a href="<?php the_permalink() ?>"><?php the_title() ?></a>
						</span>
					</article>
				<?php endwhile; endif; wp_reset_postdata() ?>
			</div>
			
           <div class="TitleBorder">
				<span class="CategoryTitle">Deportes</span>
				<a class="CategoryReadMore" href="<?php bloginfo('url') ?>/noticias/deportes">Ver Más noticias deportes</a>
			</div>

			<div class="Rows">
				<?php $Args = array('posts_per_page' => 1, 'category_name' => 'deportes-fijo', 'post__not_in' => $FeaturedPosts) ?>
				<?php $the_query = new WP_Query($Args) ?>
				<?php if ($the_query->have_posts()): while ($the_query->have_posts()): $the_query->the_post() ?>
					<?php (array) $idDeporte[]=get_the_ID();  ?>
					<article class="PostColumn">
						<a href="<?php the_permalink() ?>"><?php the_post_thumbnail( 'lsThumb' ) ?></a>
						<span class="PostTitle">
							<a href="<?php the_permalink() ?>"><?php the_title() ?></a>
						</span>
					</article>
				<?php endwhile; endif; wp_reset_postdata() ?>
			</div>
			<?php $arrayMergeDeporte = array_merge($FeaturedPosts,$idDeporte); ?>
			<div class="Rows">
				<?php $Args = array('posts_per_page' => 2, 'category_name' => 'deportes', 'post__not_in' => $arrayMergeDeporte) ?>
				<?php $the_query = new WP_Query($Args) ?>
				<?php if ($the_query->have_posts()): while ($the_query->have_posts()): $the_query->the_post() ?>
					<article class="PostColumn">
						<a href="<?php the_permalink() ?>"><?php the_post_thumbnail( 'lsThumb' ) ?></a>
						<span class="PostTitle">
							<a href="<?php the_permalink() ?>"><?php the_title() ?></a>
						</span>
					</article>
				<?php endwhile; endif; wp_reset_postdata() ?>
			</div>

			<div class="TitleBorder">
				<span class="CategoryTitle">Cultura</span>
				<a class="CategoryReadMore" href="<?php bloginfo('url') ?>/noticias/cultura">Ver Más noticias cultura</a>
			</div>

			<div class="Rows">
				<?php $Args = array('posts_per_page' => 1, 'category_name' => 'cultura-fijo', 'post__not_in' => $FeaturedPosts) ?>
				<?php $the_query = new WP_Query($Args) ?>
				<?php if ($the_query->have_posts()): while ($the_query->have_posts()): $the_query->the_post() ?>
					<?php (array) $idCultura[]=get_the_ID();  ?>
					<article class="PostColumn">
						<a href="<?php the_permalink() ?>"><?php the_post_thumbnail( 'lsThumb' ) ?></a>
						<span class="PostTitle">
							<a href="<?php the_permalink() ?>"><?php the_title() ?></a>
						</span>
					</article>
				<?php endwhile; endif; wp_reset_postdata() ?>
			</div>
			<?php $arrayMergeCultura = array_merge($FeaturedPosts,$idCultura); ?>
			<div class="Rows">
				<?php $Args = array('posts_per_page' => 2, 'category_name' => 'cultura', 'post__not_in' => $arrayMergeCultura) ?>
				<?php $the_query = new WP_Query($Args) ?>
				<?php if ($the_query->have_posts()): while ($the_query->have_posts()): $the_query->the_post() ?>
					<article class="PostColumn">
						<a href="<?php the_permalink() ?>"><?php the_post_thumbnail( 'lsThumb' ) ?></a>
						<span class="PostTitle">
							<a href="<?php the_permalink() ?>"><?php the_title() ?></a>
						</span>
					</article>
				<?php endwhile; endif; wp_reset_postdata() ?>
			</div>

			<div class="RowProgramOpinion">

				<div class="ProgramTitle">
					<span class="ProgramBack">Programas Más Vistos</span>
				</div>

				<div class="ProgramMoreView">

					<div class="ProgramRows">
						<?php $Args = array('posts_per_page' => 6, 'category_name' => 'programas', 'post__not_in' => $FeaturedPosts) ?>
						<?php $the_query = new WP_Query($Args) ?>
						<?php if ($the_query->have_posts()): while ($the_query->have_posts()): $the_query->the_post() ?>
							<article class="PostColumn">
								<a href="<?php the_permalink() ?>"><?php the_post_thumbnail( 'lsThumb' ) ?></a>
								<span class="PostTitle">
									<a href="<?php the_permalink() ?>"><?php the_title() ?></a>
								</span>
							</article>
						<?php endwhile; endif; wp_reset_postdata() ?>
					</div>


				</div>

				<div class="OpinionTitle">
					<span class="OpinionBack">Opinión</span>
				</div>

					<div class="Opinion">
						<div class="OpinionRows">
							<?php $Args = array('posts_per_page' => 3, 'category_name' => 'opinion', 'post__not_in' => $FeaturedPosts) ?>
							<?php $the_query = new WP_Query($Args) ?>
							<?php if ($the_query->have_posts()): while ($the_query->have_posts()): $the_query->the_post() ?>
								<article class="PostColumn">
									<a href="<?php the_permalink() ?>"><?php the_post_thumbnail( 'lsThumbOpinion' ) ?></a>
									<span class="PostTitle">
										<a href="<?php the_permalink() ?>"><?php the_title() ?></a>
									</span>
								</article>
							<?php endwhile; endif; wp_reset_postdata() ?>
						</div>
						<div class="OpinionReadMore">
							<a class="CategoryReadMore" href="<?php bloginfo('url') ?>/noticias/opinion">Ver Más</a>
						</div>
					</div>					
			</div>

		<div class="ContentScience">

			<div class="ContentOne">
				<?php dynamic_sidebar('index-banner-2'); ?>

				<div class="TitleBorder">
					<span class="CategoryTitle">Salud, Ciencia, Tecnología y Curiosidades</span>
					<a class="CategoryReadMore" href="<?php bloginfo('url') ?>/noticias/saludcienciatecnologia">Ver Más noticias Salud, Ciencia, Tecnología y Curiosidades</a>
				</div>

				<div class="Rows">
					<?php $Args = array('posts_per_page' => 1, 'category_name' => 'saludcienciatecnologia-fijo', 'post__not_in' => $FeaturedPosts) ?>
					<?php $the_query = new WP_Query($Args) ?>
					<?php if ($the_query->have_posts()): while ($the_query->have_posts()): $the_query->the_post() ?>
						<?php (array) $idSaludcienciatecnologia[]=get_the_ID();  ?>
						<article class="PostColumn">
							<a href="<?php the_permalink() ?>"><?php the_post_thumbnail( 'lsThumb' ) ?></a>
							<span class="PostTitle">
								<a href="<?php the_permalink() ?>"><?php the_title() ?></a>
							</span>
						</article>
					<?php endwhile; endif; wp_reset_postdata() ?>
				</div>
				<?php $arrayMergeSaludcienciatecnologia = array_merge($FeaturedPosts,$idSaludcienciatecnologia); ?>
				<div class="Rows">
					<?php $Args = array('posts_per_page' => 2, 'category_name' => 'saludcienciatecnologia', 'post__not_in' => $arrayMergeSaludcienciatecnologia) ?>
					<?php $the_query = new WP_Query($Args) ?>
					<?php if ($the_query->have_posts()): while ($the_query->have_posts()): $the_query->the_post() ?>
						<article class="PostColumn">
							<a href="<?php the_permalink() ?>"><?php the_post_thumbnail( 'lsThumb' ) ?></a>
							<span class="PostTitle">
								<a href="<?php the_permalink() ?>"><?php the_title() ?></a>
							</span>
						</article>
					<?php endwhile; endif; wp_reset_postdata() ?>
				</div>

				<?php dynamic_sidebar('index-banner-3'); ?>
			</div>

			<div class="ContentTwo"><?php dynamic_sidebar('index-banner-4'); ?></div>

		</div>



		<div class="ContentGallery">

			<div class="TitleGallery">Galería de fotos</div>
			<div class="ColumnOneGallery">
						<?php $Args = array('posts_per_page' => 1, 'meta_key' =>'GalleryPost', 'post__not_in' => $FeaturedPosts, 'post__not_in' => get_option( 'sticky_posts') ) ?>
						<?php $the_query = new WP_Query($Args) ?>
						<?php if ($the_query->have_posts()): while ($the_query->have_posts()): $the_query->the_post() ?>
							<article class="PostColumn">
								<a href="<?php the_permalink() ?>"><?php the_post_thumbnail( 'lsThumbGalleryMaxGalleryPicOne' ) ?></a>
							</article>
							<span class="PostTitleGalleryOne">
								<a href="<?php the_permalink() ?>"><?php the_title() ?></a>
							</span>
						<?php endwhile; endif; wp_reset_postdata() ?>
			</div>
			<div class="ColumnTwoGallery">
						<?php $Args = array('posts_per_page' => 1, 'meta_key' =>'GalleryPost', 'post__not_in' => $FeaturedPosts, 'post__not_in' => get_option( 'sticky_posts'), 'offset' => 1) ?>
						<?php $the_query = new WP_Query($Args) ?>
						<?php if ($the_query->have_posts()): while ($the_query->have_posts()): $the_query->the_post() ?>
							<article class="PostColumn">
								<a href="<?php the_permalink() ?>"><?php the_post_thumbnail( 'lsThumbGalleryMaxPicTwo' ) ?></a>
							</article>
							<span class="PostTitleGalleryTwo">
								<a href="<?php the_permalink() ?>"><?php the_title() ?></a>
							</span>
						<?php endwhile; endif; wp_reset_postdata() ?>
			</div>	
			<div class="ColumnThreeGallery">
						<?php $Args = array('posts_per_page' => 1, 'meta_key' =>'GalleryPost', 'post__not_in' => $FeaturedPosts, 'post__not_in' => get_option( 'sticky_posts'), 'offset' => 2) ?>
						<?php $the_query = new WP_Query($Args) ?>
						<?php if ($the_query->have_posts()): while ($the_query->have_posts()): $the_query->the_post() ?>
							<article class="PostColumnThree">
								<a href="<?php the_permalink() ?>"><?php the_post_thumbnail( 'lsThumbGalleryMaxPicTwo' ) ?></a>
							</article>
							<span class="PostTitleGalleryThree">
								<a href="<?php the_permalink() ?>"><?php the_title() ?></a>
							</span>
						<?php endwhile; endif; wp_reset_postdata() ?>
			</div>			

		</div>


			<div class="TitleBorderVenezuelans">
				<span class="CategoryTitleVenezuelans">Indicadores Económicos</span>
			</div>
			<div class="MarqueeWArea">
				<?php $economia = array('category_name' => 'indicadores', 'posts_per_page' => 10); ?>
				<div class="MarqueeEconomic" data-duplicated="false" data-pauseonhover="true" data-duration="30000" data-delaybeforestart="0">
					<?php $the_query = new WP_Query($economia) ?>
					<?php if ($the_query->have_posts()): while ($the_query->have_posts()): $the_query->the_post() ?>
						<a href="<?php the_permalink() ?>" title="<?php the_title(); ?>">
	                    	<?php the_title(); ?>
	                	</a>
					<?php endwhile; endif; wp_reset_postdata() ?>
				</div>
			</div>

			<div class="TitleBorderVenezuelans">
				<span class="CategoryTitleVenezuelans">Venezolanos</span>
			</div>

			<div class="RowsVenezuelans">
				<?php dynamic_sidebar('index-banner-5'); ?>
			</div>


			<div class="ContentDiary">
				<div class="Diary">
					<div class="ColumnOneDiary">
						<?php $Args = array('posts_per_page' => 1, 'category_name' => 'agenda', 'post__not_in' => $FeaturedPosts) ?>
						<?php $the_query = new WP_Query($Args) ?>
						<?php if ($the_query->have_posts()): while ($the_query->have_posts()): $the_query->the_post() ?>
							<article class="PostColumn">
								<a href="<?php the_permalink() ?>"><?php the_post_thumbnail( 'lsThumb' ) ?></a>
								<span class="MainPostTitle"><p><?php the_title() ?></p></span>
								<span class="MainPostExcerpt"><p><?php the_excerpt() ?></p></span>
							</article>
						<?php endwhile; endif; wp_reset_postdata() ?>
					</div>

					<div class="ColumnTwoDiary">
						<?php $Args = array('posts_per_page' => 1, 'category_name' => 'agenda', 'offset' => 1, 'post__not_in' => $FeaturedPosts) ?>
						<?php $the_query = new WP_Query($Args) ?>
						<?php if ($the_query->have_posts()): while ($the_query->have_posts()): $the_query->the_post() ?>
							<article class="PostColumn">
								<a href="<?php the_permalink() ?>"><?php the_post_thumbnail( 'lsThumb' ) ?></a>
								<span class="MainPostTitle"><p><?php the_title() ?></p></span>
								<span class="MainPostExcerpt"><p><?php the_excerpt() ?></p></span>
							</article>
						<?php endwhile; endif; wp_reset_postdata() ?>
					</div>

					<div class="ColumnThreeDiary">
						<?php $Args = array('posts_per_page' => 1, 'category_name' => 'agenda', 'offset' => 2, 'post__not_in' => $FeaturedPosts) ?>
						<?php $the_query = new WP_Query($Args) ?>
						<?php if ($the_query->have_posts()): while ($the_query->have_posts()): $the_query->the_post() ?>
							<article class="PostColumn">
								<a href="<?php the_permalink() ?>"><?php the_post_thumbnail( 'lsThumb' ) ?></a>
								<span class="MainPostTitle"><p><?php the_title() ?></p></span>
								<span class="MainPostExcerpt"><p><?php the_excerpt() ?></p></span>
							</article>
						<?php endwhile; endif; wp_reset_postdata() ?>
					</div>
				</div>

				<div class="BannerDiary">
					<?php dynamic_sidebar('index-banner-6'); ?>
				</div>				
			</div>

			<div class="RowsVenezuelans">
				<?php dynamic_sidebar('index-banner-7'); ?>
			</div>				
	
</div>
	<div class="Sidebar"><?php get_sidebar() ?></div>
</div>




<?php get_footer() ?>
