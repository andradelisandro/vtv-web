<?php 

	/**
		@author:  Coordinación Web MIPPCI
		@version: 1.0
	 */

?>

<?php get_header() ?>

<?php nx_set_post_views(get_the_ID())?>
<?php $category = single_cat_title('',false) ?>

<?php $id_category=get_query_var('cat');?>
<?php $lb_valido=''; ?>
<section class="Category">
	<div class="CategoryContent">
		<div class="CategoryTitleDefault CategoryTitle<?php echo get_query_var('cat'); ?>"><?php echo $category ?></div>
		<?php $args = array('posts_per_page' => 10,'cat'=>$id_category, 'ignore_sticky_posts'=>1, 'paged' => $paged) ?>
		<?php $the_query = new WP_Query($args);  ?>
		<?php if ($the_query->have_posts()): while ($the_query->have_posts()): $the_query->the_post() ?>

		<?php if (has_post_thumbnail()){
			$img='Img';
		}else{
			$img='WithoutImg';

		}
		?>
		<div class="CatPost<?php echo $img; ?>">
			<article class="CatPostColumnOne">
				<a class="CatThumbnail" href="<?php the_permalink() ?>" title="<?php the_title() ?>">
					<?php the_post_thumbnail( array (200, 200) ) ?>
				</a>
			</article>
			<article class="CatPostColumnTwo">
				<div class="CatPostInfo">			
					<span class="CatPost">
						<a href="<?php the_permalink() ?>">
							<?php the_title() ?>
						</a>
					</span>
					<span class="CatPostExcerpt">
						<?php the_excerpt() ?>
						<div class="TitleSeeMore">
							<span class="ContentSeeMore">
								<a href="<?php the_permalink() ?>" title="<?php the_title() ?>">
									<p>Ver Más</p>
								</a>
							</span>					
						</div>
					</span>
				</div>
			</article>
		</div>
		<?php endwhile; endif; wp_reset_postdata() ?>
		<div class="NavPag"><?php the_pagination() ?></div>
	</div>
	<div class="SidebarCategory">
		<div class="Block">
			<div class="Inner">
				<div class="LineView">
					<div class="BlockTitle"><span class="TitleLastView">Lo Último</span></div>
				</div>
					<div class="BlockContent">
				
					<?php $args=array('post__not_in' => array(get_the_ID()), 'showposts'=>5,'ignore_sticky_posts'=>1); ?>
					<?php $the_query = new WP_Query($args); 
					?>
					<?php if( $the_query->have_posts() ) { ?>
					        <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
						       <div class="Title">
						        	<a href="<?php the_permalink() ?>" title="<?php the_title(); ?>">
					                    <?php the_title(); ?>
					                </a>
					            </div>
				    		<?php endwhile; ?>
				    <?php } wp_reset_postdata()?>			
					
						</div>
			</div>
		</div>

		<div class="Block">
			<div class="Inner">
				<div class="LineView">
					<div class="BlockTitle"><span class="TitleLastView">Lo Más Leído</span></div>
				</div>
				<div class="BlockContent">				
					<?php 
					/*$args = array(
			        'posts_per_page' => 5, 
			        'meta_key' => 'nx_set_post_views', 
			        'orderby' => 'meta_value_num', 
			        'order' => 'DESC',
			        'ignore_sticky_posts'=>1
					);
			        */
			        
			        $nr = 7;

		            $sticky = get_option( 'sticky_posts' );
		            rsort( $sticky );
		            $sticky = array_slice( $sticky, 0, $nr );
			    
	            //$the_query = new WP_Query( $args );
				$the_query = new WP_Query( array( 'post__in' => $sticky, 'ignore_sticky_posts' => 1 ) );
	            if($the_query->have_posts()): while($the_query->have_posts()): $the_query->the_post();
	               ?>
					<div class="Title">
			        	<a href="<?php the_permalink() ?>" title="<?php the_title(); ?>">
		                    <?php the_title(); ?>
		                </a>
					 </div>  
       			<?php endwhile; endif; wp_reset_postdata(); ?>		
					
				</div>
			</div>
		</div>
	</div>	
</section>	
<?php get_footer() ?>