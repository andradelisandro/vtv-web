<?php 

	/**
		@author: Coordinación Web MIPPCI
		@file: search-base.php
		@version: 1.0
	 */

?>

<section class="Search">
	
	<div class="SearchForm">
		
		<div class="Inner">
			<form class="Form" method="get" action="<?php bloginfo('url') ?>">
				<input type="text" class="SearchInput" id="s" name="s" placeholder="Buscar..." autocomplete="off" value="<?php the_search_query(); ?>">
				<input class="SearchButton" type="submit" value=""></input>
			</form>
		</div>

	</div>

	<div class="SearchResults">

		<div class="Inner">
			
			<?php if (have_posts()): while(have_posts()): the_post() ?>

			<article class="Post">
				<div class="Inner">
					<a class="PostTitle" href="<?php the_permalink() ?>"><?php the_title() ?></a>
					<a class="PostExcerpt" href="<?php the_permalink() ?>"><?php the_excerpt() ?></a>
				</div>
			</article>

			<?php endwhile; else: ?>
			<p>Lo sentimos no hemos encontrado lo que buscabas</p>
			<?php endif; ?>

			<div class="Nav"><?php the_pagination() ?></div>

		</div>

	</div>

</section>